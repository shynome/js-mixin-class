
declare const MixinClass: (ClassName:string)=><F>(MixinClassFunc:F)=>F

export = MixinClass
