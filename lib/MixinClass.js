//@ts-nocheck

module.exports = ClassName => MixinClassFunc =>{
  return B => {
    let oldMixins = /**@type {string[]} */(B.__mixins || [])
    if(oldMixins.includes(ClassName)){
      return B
    }
    let newClass = MixinClassFunc(B)
    newClass.__mixins = oldMixins.concat(ClassName)
    return newClass
  }
}

