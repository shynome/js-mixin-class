const tsConfig = require('../lib/tsconfig.json')
const tsConfigPath = require('tsconfig-paths')
const path = require('path')

const cleanup = tsConfigPath.register({
  baseUrl: path.dirname(require.resolve('../lib/tsconfig.json')),
  paths: tsConfig.compilerOptions.paths,
})
// cleanup()
