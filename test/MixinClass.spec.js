const { deepStrictEqual } = require("assert");

const MixinClass = require("../lib/MixinClass");

describe('MininClass',()=>{
  it('run',()=>{
    class A {
      static A(){}
    }

    let BMixin = MixinClass('B')(/**@param {typeof A} A */(A) => class B extends A {
      static B(){}
    })
    
    let CMixin = MixinClass('C')(/**@param {typeof A} A */(A) => class C extends A {
      static C(){}
    })

    let DMixinClass = CMixin(BMixin(A))
    
    deepStrictEqual(
      ["B","C"],
      // @ts-ignore
      DMixinClass.__mixins
    )
    
    let EMixinClass = [CMixin,BMixin,CMixin,BMixin,BMixin].reduce((t,f)=>f(t),A)
    
    deepStrictEqual(
      ["B","C"],
      // @ts-ignore
      EMixinClass.__mixins.sort()
    )
    
  })
  it('type',()=>{
    // 暂时类型测试还过不了
    return 
    const { code, stderr, stdout } = require('shelljs').exec('tsc -p test')
    deepStrictEqual(
      0,
      code,
      stdout
    )
  })
})
