import MixinClass = require("MixinClass");

class A {
  static A (){}  
}

let BMixin = MixinClass('B')((a) => class B extends a {
  static B(){}
})


let CMixin = MixinClass('C')((a) => class C extends a {
  static C(){}
})

let B = BMixin(A)
let C = CMixin(A)

let DMixinClass = CMixin(BMixin(A))

DMixinClass.B
